import { Week } from '../ui/Week'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'

const mapStateToProps = (state) => ({
	forecast: state.forecast
});

export const WeekWrap = withRouter(connect(mapStateToProps)(Week));