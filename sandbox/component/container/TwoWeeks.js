import { TwoWeeks } from '../ui/TwoWeeks'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'

const mapStateToProps = (state) => ({
	forecast: state.forecast
});

export const TwoWeeksWrap = withRouter(connect(mapStateToProps)(TwoWeeks));