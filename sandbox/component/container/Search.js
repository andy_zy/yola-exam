import { connect } from 'react-redux'

import { Search } from '../ui/Search'
import { fetchForecast } from '../../js/actions'

const mapStateToProps = (state, props) => ({});

const mapDispatchToProps = dispatch => ({
	getForecast(value) {
		dispatch(
			fetchForecast(value)
		)
	}
});

export const SearchWrap = connect(mapStateToProps, mapDispatchToProps)(Search);