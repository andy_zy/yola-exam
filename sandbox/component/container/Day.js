import { Day } from '../ui/Day'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'

const mapStateToProps = (state) => ({
	forecast: state.forecast
});

export const DayWrap = withRouter(connect(mapStateToProps)(Day));
