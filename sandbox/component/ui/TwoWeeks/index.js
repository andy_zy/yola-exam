import { Bar, Tabs, Chart } from '../../../js/components'
import configs from '../../../json/charts.config.json'

export const TwoWeeks = ({forecast}) => (
	<div>
		<Bar data={forecast}/>
		<Tabs/>
		<div className="details">
			{configs.map((config, i) => <Chart key={i} config={config} days={forecast.list.slice(0, 14)}/>)}
		</div>
	</div>
);