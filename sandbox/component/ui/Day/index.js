import './style.styl'

import { Bar, Tabs } from '../../../js/components'

export const Day = ({forecast}) => {
	const today = forecast.list[0];

	return (<div>
		<Bar data={forecast}/>
		<Tabs/>
		<div className="details flex-box">
			<div className="temp flex-item">
				<ul className="clear">
					<li className="heading"><b>Temperature:</b> <span><sup>&#176;</sup>C</span></li>
					<li><b>Morning</b> <span>{today.temp.morn}</span></li>
					<li><b>Day</b> <span>{today.temp.day}</span></li>
					<li><b>Night</b> <span>{today.temp.night}</span></li>
					<li><b>Min</b> <span>{today.temp.min}</span></li>
					<li><b>Max</b> <span>{today.temp.max}</span></li>
				</ul>
			</div>
			<div className="summary flex-item">
				<ul className="clear">
					<li>&nbsp;</li>
					<li><b>Pressure</b> <span>{today.pressure} hPa</span></li>
					<li><b>Humidity</b> <span>{today.humidity} %</span></li>
					<li><b>Wind Speed</b> <span>{today.speed} m/s</span></li>
					<li><b>Clouds</b> <span>{today.clouds} %</span></li>
					{today.rain ? <li><b>Rain</b> <span>{today.rain} %</span></li> : null}
				</ul>
			</div>
		</div>
	</div>)
};