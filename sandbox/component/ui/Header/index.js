import './style.styl'
import logo from './img/logo.png'

import { SearchWrap } from '../../../js/components'

export const Header = (props) => (
	<header className="header">
		<div className="inner clear">
			<a className="logo" href="/"><img src={logo}/> <span>Forecast</span></a>
			<SearchWrap {...props}/>
		</div>
	</header>
);
