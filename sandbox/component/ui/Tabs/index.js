import './style.styl'
import { Link } from 'react-router'

const tabs = [
	{
		name: 'Today',
		href: 'day'
	},
	{
		name: 'Week',
		href: 'week'
	},
	{
		name: 'Two weeks',
		href: 'two-weeks'
	}
];

export const Tabs = (props) => (
	<div className="tabs clear">
		<ul className="flex-box">
			{tabs.map((tab, i) => <li key={i} className="flex-item"><Link activeClassName="active" to={tab.href}>{tab.name}</Link></li>)}
		</ul>
	</div>
);