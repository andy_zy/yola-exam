import img from './img/404oops.jpg'

export const Oops = () => <img src={img} alt="Oops"/>;