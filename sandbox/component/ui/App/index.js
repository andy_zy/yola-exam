import { Provider } from 'react-redux'
import { Router, Route, hashHistory, Redirect } from 'react-router'
import { MainFrame, DayWrap, WeekWrap, TwoWeeksWrap, Hint, Oops } from '../../../js/components'

import './reset.css'
import './style.styl'

import storeFactory from '../../../store'
import sampleData from '../../../json/initianState.json'

const initialState = (localStorage["redux-store"]) ? JSON.parse(localStorage["redux-store"]) : sampleData;
const store = storeFactory(initialState);
const saveState = () => localStorage["redux-store"] = JSON.stringify(store.getState());

store.subscribe(saveState);

export const App = () => (
	<Provider store={store}>
		<Router key={Math.random()} history={hashHistory}>
			<Redirect from="/" to="/search"/>
			<Route path="/" component={MainFrame}>
				<Route path="search" component={Hint}/>
				<Route path="day" component={DayWrap}/>
				<Route path="week" component={WeekWrap}/>
				<Route path="two-weeks" component={TwoWeeksWrap}/>
			</Route>
			<Route path="*" component={Oops}/>
		</Router>
	</Provider>
);
