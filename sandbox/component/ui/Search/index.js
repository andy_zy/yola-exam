import './style.styl'

export const Search = ({state, getForecast}) => {
	const trim = (string) => string.replace(/^\s+/, '').replace(/\s+$/, '');
	const search = (e) => {
		e.preventDefault();

		getForecast(trim(e.target.querySelector('#search').value));
	};

	return (
		<form className="search-form" onSubmit={search}>
			<input id="search" type="search" placeholder="City or zipcode" required/>
		</form>
	)
};