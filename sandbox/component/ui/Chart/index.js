import buildChart from '../../../js/buildChart'

export const Chart = ({days, config}) => (
    <div className="chart-box">
        <div className="chart" dangerouslySetInnerHTML={{__html: buildChart(days, config)}}></div>
    </div>
);