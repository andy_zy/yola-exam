import './style.styl'

const monthList = [
	'January',
	'February',
	'March',
	'April',
	'May',
	'June',
	'July',
	'August',
	'September',
	'October',
	'November',
	'December'
];

const weekDays = [
	'Monday',
	'Tuesday',
	'Wednesday',
	'Thursday',
	'Friday',
	'Saturday',
	'Sunday'
];

let now = new Date();
let date = now.getDate();
let dayOfWeek = now.getDay();
let month = now.getMonth();

export const Bar = ({data}) => (
	<div className="bar">
		<div className="date">
			<span>{weekDays[dayOfWeek - 1]}</span>,&nbsp;
			<span>{date}</span>&nbsp;
			<span>{monthList[month]}</span>
		</div>
		<div className="main-info">
			<div className="location">
				<span>{data.city.name}</span>,&nbsp;
				<span>{data.city.country}</span>
			</div>
			<div className="temp">
				<span>{Math.round(data.list[0].temp.eve)} <sup>&#176;</sup>C</span>
			</div>
			<div className="desc">
				<img src={`//openweathermap.org/img/w/${data.list[0].weather[0].icon}.png`} width="30" alt={data.list[0].weather[0].description}/>
				&nbsp;
				{data.list[0].weather[0].main}
			</div>
		</div>
	</div>
);