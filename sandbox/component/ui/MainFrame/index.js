import { Header } from '../../../js/components'

export const MainFrame = (props) => (
	<div className="wrapper">
		<Header {...props}/>
		<main className="container inner">
			{props.children}
		</main>
	</div>
);