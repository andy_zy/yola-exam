import getJSON from '../service/getJSON'
import { apiKey } from '../json/credentials.json'

export const addCity = (cityData) => {
	return {
		type: 'ADD_CITY',
		payload: cityData
	}
};

export const fetchForecast = value => dispatch => {
	getJSON({
		url: `//api.openweathermap.org/data/2.5/forecast/daily?q=${value}&units=metric&cnt=16&appid=${apiKey}`,
		cbName: 'openweathermap',
		doneFn: (data) => {
			dispatch(
				addCity(data)
			);

			location.hash = '#/day';
		},
		failFn: () => location.hash = '#/oops'
	});
};