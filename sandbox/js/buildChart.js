const monthList = [
	'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
];

export default function(days, config){
	setTimeout(() => {
		Highcharts.chart(config.unitProp, {
			chart: {
				type: config.chartType,
				backgroundColor: '#f5f5f5'
			},
			title: {
				text: config.unitType
			},
			xAxis: {
				gridLineWidth: 0,
				categories: days.map((day) => {
					let date = new Date(day.dt * 1000);

					return `${date.getDate()} ${monthList[date.getMonth()]}`;
				})
			},
			yAxis: {
				title: false,
				labels: false,
				gridLineWidth: 0
			},
			legend: false,
			tooltip: {
				formatter: function() {
					return `${this.series.name}<br>${this.y} ${config.unitMeasure}`;
				}
			},
			series: [{
				name: config.unitType,
				data: days.map((day) => config.unitProp === 'temp.eve' ? day.temp.eve : day[config.unitProp])
			}]
		});
	}, 0);

	return `<div id="${config.unitProp}" style="width:100%; height:300px"></div>`;
};