import '../template/index.pug'

import { render } from 'react-dom'
import { App } from './components'

document.addEventListener('DOMContentLoaded', () => {
	render(
		<App/>,
		document.querySelector('#app')
	)
}, false);