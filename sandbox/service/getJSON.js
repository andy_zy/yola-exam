/**
 * @summary JSONP data fetching
 * @param {string} url
 * @param {string} cbParam
 * @param {string} cbName
 * @param {function} doneFn
 * @param {function} failFn
 */
export default ({url, cbParam='callback', cbName, doneFn, failFn}) => {
	const toggleScriptTag = (method, tag) => document.getElementsByTagName('head')[0][`${method}Child`](tag);
	let script = document.createElement('script');

	script.type = 'text/javascript';
	script.src = `${url}${~url.indexOf('?') ? '&' : '?'}${cbParam}=${cbName}`;

	window[cbName] = (data) => {
        typeof doneFn === 'function' && doneFn(data);
		delete window[name];
        toggleScriptTag('remove', script);
	};

    script.onerror = () => {
        typeof failFn === 'function' && failFn();
        toggleScriptTag('remove', script);
    };

	toggleScriptTag('append', script);
}