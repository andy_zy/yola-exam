import { combineReducers } from 'redux'

export const forecast = (state=null, action) => {
	return (action.type === 'ADD_CITY') ? action.payload : state;
};

export default combineReducers({
	forecast
})




