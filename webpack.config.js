const NODE_ENV = process.env.NODE_ENV || 'development';

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const addHash = (template, hash) => (
    NODE_ENV === 'production' ? template.replace(/\.(.[^.]+)$/, `.[${hash}].$1`) : `${template}?hash=[${hash}]`
);

module.exports = {
    context: __dirname + '/sandbox',

    entry: {
        app: './js/index'
    },

    output: {
        path: __dirname + '/production/',
        publicPath: '/',
        filename: addHash('script/[name].js', 'hash'),
        chunkFilename: addHash('script/[name].js', 'chunkhash')
    },

    watch: NODE_ENV === 'development',

    watchOptions: {
        aggregateTimeout: 100	//increase compilation speed
    },

    devtool: NODE_ENV === 'development' ? 'source-map' : false,

    plugins: [
		new HtmlWebpackPlugin({
			filename: './index.html',
			inject: 'head',
			template: './template/index.pug',
			chunks: ['app']
		}),

		new webpack.ProvidePlugin({
			'React': 'react',
			'ReactDOM': 'react-dom',
			'Highcharts': 'highcharts'
		}),

		new ExtractTextPlugin({
			filename: addHash('style/[name].css', 'contenthash'),
			allChunks: true,
			disable: NODE_ENV === 'development'
		}),

		new webpack.NoEmitOnErrorsPlugin(),

		new webpack.DefinePlugin({
			NODE_ENV: JSON.stringify(NODE_ENV)
		})
    ],

    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader?optional[]=runtime',
                exclude: /(node_modules)/,
                query: {
                    presets: ['latest', 'stage-0', 'react']
                }
            },
            {
                test: /\.(css|styl)$/,
                exclude: /(node_modules)/,
                loader: ExtractTextPlugin.extract({
                    filename: addHash('[name].css', 'contenthash'),
                    fallback: 'style-loader',
                    use: ['css-loader', {
                    	loader: 'postcss-loader',
						options: {
							plugins: (loader) => [
								require('postcss-import')({ root: loader.resourcePath }),
								require('autoprefixer')(),
								require('cssnano')()
							],
							sourceMap: true
						}
					}, 'stylus-loader?resolve url']
                })
            },
            {
                test: /\.pug$/,
                exclude: /(node_modules)/,
                loader: 'pug-loader'
            },
            {
                test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
                exclude: /(node_modules)/,
                loader: addHash('url-loader?name=[path][name].[ext]&limit=4096', 'hash:6')
            },
			{
				test: /\.json$/,
				exclude: /(node_modules)/,
				loader: 'json-loader'
			},
        ]
    },

    devServer: {
        host: 'localhost',
        port: 8080,
        contentBase: __dirname + '/production',
        hot: true
    }
};

if( NODE_ENV === 'production' ){
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        })
    );
}